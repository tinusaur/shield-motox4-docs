# Shield MOTOx4 Docs

Shield MOTOx4 Documentation

## Tinusaur Shield MOTOx4 Assembling Guide

![Tinusaur Shield MOTOx4 Assembling Guide](Tinusaur_Shield_MOTOx4_Gen3_Assembling_Guide.jpg "Tinusaur Shield MOTOx4 Assembling Guide")

- Download link: https://gitlab.com/tinusaur/shield-motox4-docs/-/raw/master/Tinusaur_Shield_MOTOx4_Gen3_Assembling_Guide.pdf?inline=false


